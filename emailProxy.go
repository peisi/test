package main

import "bytes"
import "compress/gzip"
import "fmt"
import "io"
import "io/ioutil"
import "log"
import "net/http"
import "os"
import "regexp"
import "strings"

func main() {
	xp := new(XProxy)
	http.Handle("/", xp)
	addr := ":8080"
	if len(os.Args) == 2 {
		addr = os.Args[1]
	}
	fmt.Fprintln(os.Stderr, "Linsten on ", addr)
	err := http.ListenAndServe(addr, nil)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Usage: xproxy :8080 > emails.csv ")
		fmt.Fprintln(os.Stderr, "Useful Commands:")
		fmt.Fprintln(os.Stderr, " env http_proxy=http://localhost:8080 wget -r -nd --delete-after http://www.chinajob.com")
		fmt.Fprintln(os.Stderr, "tail -f emails.csv")
		log.Fatal("ListenAndServe: ", err)
	}
}

func Process(buf *bytes.Buffer, req *http.Request, res *http.Response) {
	ctype := res.Header.Get("Content-Type")
	if ctype == "" || strings.Index(ctype, "text/html") != -1 || strings.Index(ctype, "text/plain") != -1 {
		var dg io.Reader
		if res.Header.Get("Content-Encoding") == "gzip" {
			dg, _ = gzip.NewReader(buf)
		} else {
			dg = buf
		}
		recordEmail(dg, req.RequestURI)
	} else {
		//	fmt.Println(ctype)
	}
}

const _EXP_EMAIL = `[a-z0-9._%+#\-]+@[a-z0-9.\-]+\.[a-z]{2,4}`

var emailRegexp, err = regexp.Compile(_EXP_EMAIL)
var outlock = make(chan bool, 1)

func recordEmail(s io.Reader, url string) {
	em := make(map[string]bool)
	b, _ := ioutil.ReadAll(s)
	ss := emailRegexp.FindAll(b, -1)
	outlock <- true
	for _, es := range ss {
		if _, ok := em[string(es)]; !ok {
			fmt.Println(string(es), ",", url)
			em[string(es)]=true
		}
	}
	<-outlock
}

type XProxy struct {
	Transport http.RoundTripper
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

func (p *XProxy) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	trans := p.Transport
	if trans == nil {
		trans = http.DefaultTransport
	}

	outreq := new(http.Request)
	*outreq = *req
	outreq.Proto = "HTTP/1.1"
	outreq.ProtoMajor = 1
	outreq.ProtoMinor = 1
	outreq.Close = false

	if outreq.Header.Get("Connection") != "" {
		outreq.Header = make(http.Header)
		copyHeader(outreq.Header, req.Header)
		outreq.Header.Del("Connection")
	}

	res, err := trans.RoundTrip(outreq)
	if err != nil {
		log.Printf("proxy error: %v", err)
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	copyHeader(rw.Header(), res.Header)
	rw.WriteHeader(res.StatusCode)

	if res.Body != nil {
		defer res.Body.Close()
		buf := make([]byte, 32*1024)
		sw := new(bytes.Buffer)
		for {
			nr, er := res.Body.Read(buf)
			if nr > 0 {
				sw.Write(buf[0:nr])
				rw.Write(buf[0:nr])
			}
			if er == io.EOF {
				break
			}
			if er != nil {
				log.Printf("read error: %v", er)
				break
			}
		}
		go Process(sw, req, res)
	}
}
