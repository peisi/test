<?php
define("YQL_BASE","http://query.yahooapis.com/v1/public/yql");
define("DEBUG",FALSE);
define("CRAWLERS_DIR",dirname(__FILE__)."/crawlers/");

$crawlers=array();
//$crawlers[]='craigslist';
//$crawlers[]='chinajob';
$crawlers[]='echinacities';

foreach($crawlers as $c){
	include_once CRAWLERS_DIR.$c.'.php';
}

class Job {
	var $title;
	var $email;
	var $salary;
	var $desc;
	var $city;
	var $postDate;
	var $employer;
	var $category;
	var $source;
	var $id;
	private static $records = array();

	/*
	 * Store this job to DB.
	 */
	function store(){
		//TODO:implement it with database
		$records[] = $this->source.'/'.$this->id;
		if (!$this->email){
			echo $this->source,"/",$this->id,":no email\r\n";
			return;
		}
		echo "\r\n",$this->title,"\r\n";
		echo '================',"\r\n";
		echo "\tEmail:\t",$this->email,"\r\n";
		echo "\tSalary:\t",$this->salary,"\r\n";
		echo "\tCity:\t",$this->city,"\r\n";
		echo "\tCat.:\t",$this->category,"\r\n";
		echo "\tDate\t",$this->postDate,"\r\n";
		echo $this->desc,"\r\n\r\n";
	}

	/*
	 * Return whether this job has been crawled before.
	 */
	function hasRecord(){
		//TODO:implement it with database
		if(in_array($this->source.'/'.$this->id,self::$records)){
			return true;
		}else{
			return false;
		}
	}
}

/*
 * Crawlers of each site implement JobCrawler and names starting with 'Crawler_'
 */
abstract class JobCrawler {
	public function start(){
		while($this->nextIndex()){
		}
	}

	/**
	 * Process next page. Return true for there are further page need to crawle;
	 * 	false for stop.
	 */
	abstract public function nextIndex();

	/*
	 * Query page using YQL string. Query result returned as a SimpleXMLElement object.
	 */
	static public function queryPage($q){
		$ch=curl_init(YQL_BASE.'?q='.urlencode($q).(DEBUG?'&diagnostics=true&debug=true':''));
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		$xml=curl_exec($ch);
		if(DEBUG){
			dlog($xml);
		}
		try {
			$x=new SimpleXMLElement($xml);
			$result=$x->xpath('/query/results');
			if(!$result || count($result)<1){
				dlog($xml);
				return false;
			}
			return $result[0];
		}catch (Exception $e){
			dlog($xml);
		}
		return FALSE;
	}

	/*
	 * Get salary from a string. False for not found.
	 */
	static public function parseSalary($s){
		//TODO: make a more sophisticated pattern.
		preg_match('/\b[0-9 -.]*RMB[0-9 -.]*\b/',$s,$matches);
		if($matches){
			$salary=$matches[0];
			return $salary;
		}
		return false;
	}

	/*
	 * Get email address from a string. False for not found.
	 */
	static public function parseEmail($s){
		//TODO: make a more sophisticated pattern.
		preg_match('/\b[0-9 -.]*RMB[0-9 -.]*\b/',$s,$matches);
		preg_match('/\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b/',
			$s, $matches);
		if($matches){
			$email = $matches[0];
			return $email;
		}
		return false;

	}
}

function dlog($s){
	echo $s,"\r\n";
}

/*
 * get each class implements JobCrawler named as 'Crawler_*',  than call start() method of it's object.
 */
function go(){
	$cs= get_declared_classes();
	foreach($cs as $c){	
		if(strpos($c,'Crawler_')===0 &&	is_subclass_of($c,'JobCrawler')){
			$crawler=new $c();
			$crawler->start();
		}
	}
}

//run from command line$ php -f crawler.php
go();
