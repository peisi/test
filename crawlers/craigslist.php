<?php
/*
 * craigslist.com job crawler implement.
 */
class Crawler_Craigslist extends JobCrawler {
	var $cities = array();
	var $citiURL = array();
	var $nextPage='';
	var $cityIndex=0;

	function __construct(){
		$result=self::queryPage1('http://www.craigslist.com.cn','//div[@id="list"]/a');
		if($result){
			foreach($result as $a){
				$this->cities[]=(string)$a;
				$this->citiURL[]=(string)$a['href'];
			}
		}
	}
	function nextIndex() {
		$nomore=FALSE;
		$nextPage=FALSE;
		$result=self::queryPage1($this->citiURL[$this->cityIndex].'jjj/'.$this->nextPage,'//p[@class="row"]/a/@href | //p[@id="nextpage"]/font/a/@href');
		if($result){
			foreach($result as $a){
				if(strstr($a['href'], $this->citiURL[$this->cityIndex])){
					$job=$this->getJob($a['href']);
					if($job){
						if($job->hasRecord()){
							$nomore=TRUE;
							break;
						}else{
							$job->store();
						}
					}
				}elseif(strstr($a['href'],'index')){
					$nextPage=$a['href'];
				}
			}
			if($nomore || $nextPage===FALSE){
				if(count($this->cities)==$this->cityIndex){
					return false;
				}  
				$this->cityIndex++;
				$this->nextPage='';
				echo $this->cities[$this->cityIndex],"\r\n";
			}else{
				$this->nextPage=$nextPage;
				echo $nextPage,"\r\n";
			}
		}
		return true;
	}
	function getJob($url){
		$result=self::queryPage1($url,'//h2 | //span[@class="postingdate"] | //span[@class="returnemail"]/a | //div[@id="userbody"]');
		if($result){
			$job=new Job;
			$job->source='craigslist';
			$job->id=$this->cities[$this->cityIndex].'/'.$url;
			$job->email=(string)$result[2];
			$job->title= (string)$result[0];
			$job->postDate=(string)$result[1];
			$job->city=$this->cities[$this->cityIndex];		
			$job->salary=self::parseSalary($result[3]);
			if(!$job->salary){
				$job->salary=self::parseSalary($job->title);
			}
			$job->desc=(string)$result[3];
			return $job;
		}else{
			return false;
		}
	}
	static function queryPage1($url,$xpath){
		$ch=curl_init($url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
		$xml=curl_exec($ch);
		$DOM = new DOMDocument();
		libxml_use_internal_errors(FALSE);
		if(! $DOM->loadHTML($xml)){
			return false;
		}
		$x=simplexml_import_dom($DOM);
		$result=$x->xpath($xpath);
		return $result;
	}

}
