<?php
/*
 * chinajob.com job crawler implement.
 */
class Crawler_ChinaJob extends JobCrawler {
	const listQuery = 'select href from html where url="http://www.chinajob.com/individual/my_%slist.php?offset=%d" and xpath=\'//th[@class="STYLE11"]/a\'';
	const jobQuery = 'select * from html where url="http://www.chinajob.com%s%d" and xpath=\'//td[@bgcolor="#68B5DF"]/font | //td[@bgcolor="#68B5DF"]/../../child::tr[position()>3 and position()<last()-2]/td/p\'';
	var $lists=array("job","teacher");
	var $listDetail=array("/individual/my_jobdetail.php?job_id=","/jobposter/teacher/jobdetail.php?job_id=");
	var $pageID=1;
	var $listIndex=0;

	function nextIndex(){
		$nomore=false;
		$result=self::queryPage(sprintf(self::listQuery,$this->lists[$this->listIndex],$this->pageID));
		if($result){
			foreach($result->a as $a){
				preg_match('/[0-9]+/',$a['href'],$matches);
				if($matches){
					$id=$matches[0];
					$job=$this->getJob($this->listIndex,$id);
					if($job){
						if($job->hasRecord()){
							$nomore=true;
							break;
						}else{
							$job->store();
						}
					}
				}
			}
			if($nomore){
				if($this->listIndex==count($this->lists)-1){
					return false;
				}else{
					$this->listIndex++;
					$this->pageID=1;
				}
			}else{
				$this->pageID++;
			}
			return true;
		}else{
			return false;
		}
	}

	function getJob($list,$id){
		$result=self::queryPage(sprintf(self::jobQuery,$this->listDetail[$list],$id));
		if($result){
			$job=new Job;
			$job->source='chinajob';
			$job->category=$this->lists[$list];
			$job->id=$this->lists[$list].'/'.$id;
			$job->title=trim($result->font,' ');
			$startDesc=FALSE;
			foreach($result->p as $p){
				$ss=explode(':',$p,2);
				if(count($ss)==2){
					switch(trim($ss[0],' ')){
					case 'Email':
						$job->email=self::parseEmail($ss[1]);
						break;
					case 'Publish Date':
						$job->postDate=trim($ss[1],' ');
						break;
					case 'Job Location':
					case 'School location':
						$job->city=trim($ss[1],' ');
						break;
					case 'Job Salary':
					case 'Salary':
						$job->salary=self::parseSalary($ss[1]);
						break;
					case 'Job Description':
					case 'Class Subject':
						$startDesc=TRUE;
					default:
						if(!$job->salary){
							$job->salary=self::parseSalary($ss[1]);
						}		
						if(!$job->email){
							$job->email=self::parseEmail($ss[1]);
						}
						if($startDesc){
							$job->desc.=$p;
						}
						break;
					}
				}else{
					if($startDesc){
						$job->desc.=$p;
					}
				}
			}
			return $job;
		}else{
			return false;
		}
	}
}
