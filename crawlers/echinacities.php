<?php
/*
 * echinacities.com job clawler implement.
 */
class Crawler_EChinaCities extends jobCrawler {
	const listQuery='select * from html where url="http://jobs.echinacities.com/default-%d.html" and xpath=\'//table[@class="joblistTable"][2]/tr/td[@class="title"]/a/@href\'';
	const jobQuery='select * from html where url="http://jobs.echinacities.com/%s" and xpath=\'//div[@class="jcctitle"]/h1 | //div[@class="jobfl"]/div | //div[@class="jcccontent"]/p\'';

	var $pageID=1;

	function nextIndex(){
		//flag for whether there are job information crawled before in this page.
		//If so,crawling can be terminated.
		$nomore=false;

		$result=self::queryPage(sprintf(self::listQuery,$this->pageID));
		if($result){
			foreach($result->a as $a){
				$job=self::getJob((string)$a['href']);
				if($job){
					if($job->hasRecord()){
						$nomore=true;
						break;
					}else{
						$job->store();
					}
				}
			}
			if($nomore){
				return false;
			}else{
				$this->pageID++;
				return true;
			}
		}
		return false;
	}

	function getJob($url){
		$result=self::queryPage(sprintf(self::jobQuery,$url));
		if($result){
			$job=new Job;
			$job->source='echinacities';
			$job->id=$url;
			//div's in the header field
			foreach($result->div as $fl){
				switch ($fl->span[0]){
				case "Category:":
					$job->category=trim($fl->span[1]->a);
					break;
				case "Contact E-mail:":
					$job->email=self::parseEmail($fl->span[1]->a);
					break;
				case "Post City:":
					$job->city=trim($fl->span[1]->a);
					break;
				case "Post Date:":
					$job->postDate=trim($fl->span[1]);
					break;
				default:
					break;
				}
			}

			//paragraphs to form description
			foreach($result->p as $p){
				if(!$job->email){
					$job->email=self::parseEmail($p->asXML());
				}
				if(!$job->salary){
					$job->salary=self::parseSalary($p->asXML());
				}
				$job->desc.=$p->asXML();
			}


			//job title
			$job->title=$result->h1;

			return $job;
		}
		return false;

	}
}


